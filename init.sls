install-gitlab-runner-package:
  pkg.installed:
    - pkgs:
      - gitlab-runner

gitlab-runner:
  service:
    - running
    - enable: True
    - require:
      - pkg: install-gitlab-runner-package

{% if grains['os_family'] == 'RedHat' %}
/etc/gitlab-runner/certs/gitlab01.sys.local_ca.crt:
  file.managed:
    - source: salt://{{ tpldir }}/files/gitlab01.sys.local.crt
    - makedirs: True
    - user: root
    - group: root
    - mode: '0755'
    - require_in:
      - service: gitlab-runner
{% endif %}

{%- if 'gitlab-runner-docker' in pillar['roles'] %}
/etc/cron.d/gitlab-runner-docker:
  file.managed:
    - source: salt://{{ tpldir }}/files/cronfile
    - user: root
    - group: root
    - mode: '0644'
{%- endif %}
